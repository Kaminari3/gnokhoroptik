from django.urls import path
from django.conf.urls import url, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from . import views
from rest_framework import routers


router = routers.DefaultRouter()



urlpatterns = [
    path('', views.index, name='index'),
    url(r'^articles/$', views.ArticleListCreateView.as_view()),
    url(r'^articleupdate/(?P<id>\d)/$', views.ArticleUpdateView.as_view()),
    url(r'^articledelete/(?P<pk>[0-9]+)/$', views.ArticleDeleteView.as_view()),
    url(r'^login/$', obtain_jwt_token),
    url(r'^category/$', views.CategoryListCreateView.as_view()),
    url(r'^categoryupdate/(?P<pk>[0-9]+)/$', views.CategoryUpdateView.as_view()),
    url(r'^categorydelete/(?P<pk>[0-9]+)/$', views.CategoryDeleteView.as_view()),
    url(r'^categoryById/$', views.CategoryById.as_view()),
    url(r'^articleById/$', views.ArticleById.as_view()),
    url(r'^articleBycategory/$', views.ArticleByCategory.as_view()),
]