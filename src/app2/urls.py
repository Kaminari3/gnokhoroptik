from django.urls import path
from django.conf.urls import url, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()


urlpatterns = [
    path(r'', views.index, name='index'),
    url(r'^products/$',views.ProductView.as_view()),
]