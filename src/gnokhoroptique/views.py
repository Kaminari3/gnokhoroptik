from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import generics, permissions, status
from gnokhoroptique.models import *
from gnokhoroptique.serializers import *
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.views import APIView
# Create your views here.




def index(request):
	return HttpResponse("Bienvenue dans Gnokhor Optique")


# Vue pour afficher les clients
class ClientListView(generics.CreateAPIView):

	
	serializer_class = ClientSerializer

	def get (self, request, *args, **kwargs):
		client = Client.objects.all()

		if not client:
			return Response({
				"status":"failure",
				"message": "no such item",
				}, status= status.HTTP_404_NOT_FOUND)

		serializer = ClientSerializer(client, many=True)


		return Response({
			"status":"success",
			"message": "items successfully retrieved",
			"count": client.count(),
			"data": serializer.data
			}, status=status.HTTP_200_OK)

# Vue pour afficher les Lunettes

class LunetteListView(generics.CreateAPIView):

	
	serializer_class = LunetteSerializer

	def get (self, request, *args, **kwargs):
		lunette = Lunettes.objects.all()

		if not lunette:
			return Response({
				"status":"failure",
				"message": "no such item",
				}, status= status.HTTP_404_NOT_FOUND)

		serializer = LunetteSerializer(lunette, many=True)


		return Response({
			"status":"success",
			"message": "items successfully retrieved",
			"count": lunette.count(),
			"data": serializer.data
			}, status=status.HTTP_200_OK)

# Vue pour afficher les Commandes

class CommandeListView(generics.CreateAPIView):

	
	serializer_class = CommandeSerializer

	def get (self, request, *args, **kwargs):
		commande = Commande.objects.all()

		if not commande:
			return Response({
				"status":"failure",
				"message": "no such item",
				}, status= status.HTTP_404_NOT_FOUND)

		serializer = CommandeSerializer(commande, many=True)


		return Response({
			"status":"success",
			"message": "items successfully retrieved",
			"count": commande.count(),
			"data": serializer.data
			}, status=status.HTTP_200_OK)


# vue pour ajouter un client
class ClientListAddView(generics.CreateAPIView):
	
	serializer_class = ClientSerializer

	def post (self, request, *args, **kwargs):
		
		serializer = ClientSerializer(data=request.data)
		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": serializer.errors,
				"errors": "erreur"
				}, status= status.HTTP_400_BAD_REQUEST)

		
		

		serializer.save()  #save plutot que create method



		return Response({
			"status":"success",
			"message": "client successfully created",
			
		}, status=status.HTTP_201_CREATED)

# vue pour modifier un client

class ClientUpdateView(generics.CreateAPIView):

	serializer_class = ClientSerializer

	def put (self, request, *args, **kwargs):
		try:
			client = Client.objects.get(id=kwargs['id'])
		except Client.DoesNotExist:
			return Response({
				"status":"failure",
				"message": "no such item",
			}, status=status.HTTP_400_BAD_REQUEST)

		client_data= {
			'nom': request.data['nom'],
			'prenom': request.data['prenom'],
			'adresse': request.data['adresse'],
			'telephone': request.data['telephone'],
		}

		serializer = ClientSerializer(client, data=client_data, partial=True)

		if not serializer.is_valid():
			return Response({
				"status": "failure",
				"message": "invalid data",
				"errors": serializer.errors
				}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status":"success",
			"message": "client successfully update",
			"data": serializer.data
			}, status=status.HTTP_200_OK)


# vue pour supprimer un client

class ClientDeleteView (generics.CreateAPIView):
	
	serializer_class = ClientSerializer

	#d'abord montrer la liste des clients pour qu'on puisse fill les champs

	def get (self, request, *args, **kwargs):
		client = Client.objects.all()

		if not client:
			return Response({
				"status":"failure",
				"message": "no such item",
				}, status= status.HTTP_404_NOT_FOUND)

		serializer = ClientSerializer(client, many=True)


		return Response({
			"status":"success",
			"message": "items successfully retrieved",
			"count": client.count(),
			"data": serializer.data
			}, status=status.HTTP_200_OK)


		#et là on peut regarder quoi supprimer


	def post(self, request, *args, **kwargs):
		try:
			client = Client.objects.get(id=kwargs['pk'])
		except Client.DoesNotExist:
			return Response({
				"status": "failure",
				"message": "no such item"
				}, status=status.HTTP_400_BAD_REQUEST)

		client.delete()
		return Response({
			"status": "success",
			"message": "client successfully deleted"
			}, status=status.HTTP_200_OK)


# vue pour ajouter des lunettes
class LunetteListAddView(generics.CreateAPIView):
	
	serializer_class = LunetteSerializer

	def post (self, request, *args, **kwargs):
		
		serializer = LunetteSerializer(data=request.data)
		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": serializer.errors,
				"errors": "erreur"
				}, status= status.HTTP_400_BAD_REQUEST)

		
		
		serializer.save()


		return Response({
			"status":"success",
			"message": "lunettes successfully created",
			
		}, status=status.HTTP_201_CREATED)

# vue pour modifier des lunettes

class LunetteUpdateView(generics.CreateAPIView):

	serializer_class = LunetteSerializer

	def put (self, request, *args, **kwargs):
		try:
			lunette = Lunettes.objects.get(id=kwargs['id'])
		except Lunettes.DoesNotExist:
			return Response({
				"status":"failure",
				"message": "no such item",
			}, status=status.HTTP_400_BAD_REQUEST)

		lunette_data= {
			'nom': request.data['nom'],
			'type': request.data['type'],
			'prix': request.data['prix'],
			
		}

		serializer = LunetteSerializer(lunette, data=lunette_data, partial=True)

		if not serializer.is_valid():
			return Response({
				"status": "failure",
				"message": "invalid data",
				"errors": serializer.errors
				}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status":"success",
			"message": "lunettes successfully update",
			"data": serializer.data
			}, status=status.HTTP_200_OK)


# vue pour supprimer des lonnettes

class LunetteDeleteView (generics.CreateAPIView):
	
	serializer_class = LunetteSerializer

	def post(self, request, *args, **kwargs):
		try:
			lunette = Lunettes.objects.get(id=kwargs['pk'])
		except Lunettes.DoesNotExist:
			return Response({
				"status": "failure",
				"message": "no such item"
				}, status=status.HTTP_400_BAD_REQUEST)

		lunette.delete()
		return Response({
			"status": "success",
			"message": "lunette successfully deleted"
			}, status=status.HTTP_200_OK)


# vue pour ajouter des commandes
class CommandeListAddView(generics.CreateAPIView):
	
	serializer_class = CommandeSerializer

	def post (self, request, *args, **kwargs):
		
		serializer = CommandeSerializer(data=request.data)
		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": serializer.errors,
				"errors": "erreur"
				}, status= status.HTTP_400_BAD_REQUEST)

		
		
		serializer.save()


		return Response({
			"status":"success",
			"message": "commande successfully created",
			
		}, status=status.HTTP_201_CREATED)

# vue pour modifier des commandes

class CommandeUpdateView(generics.CreateAPIView):

	serializer_class = CommandeSerializer

	def put (self, request, *args, **kwargs):
		try:
			commande = Commande.objects.get(id=kwargs['id'])
		except Commande.DoesNotExist:
			return Response({
				"status":"failure",
				"message": "no such item",
			}, status=status.HTTP_400_BAD_REQUEST)

		commande_data= {
			'nbre_lunettes': request.data['nbre_lunettes'],
			'lunette_id': request.data['lunette_id'],
			'montant_total': request.data['montant_total'],
			
		}

		serializer = CommandeSerializer(commande, data=commande_data, partial=True)

		if not serializer.is_valid():
			return Response({
				"status": "failure",
				"message": "invalid data",
				"errors": serializer.errors
				}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status":"success",
			"message": "commande successfully update",
			"data": serializer.data
			}, status=status.HTTP_200_OK)


# vue pour supprimer une commande

class CommandeDeleteView (generics.CreateAPIView):
	
	serializer_class = CommandeSerializer

	def post(self, request, *args, **kwargs):
		try:
			commande = Commande.objects.get(id=kwargs['pk'])
		except Commande.DoesNotExist:
			return Response({
				"status": "failure",
				"message": "no such item"
				}, status=status.HTTP_400_BAD_REQUEST)

		Commande.delete()
		return Response({
			"status": "success",
			"message": "commande successfully deleted"
			}, status=status.HTTP_200_OK)



