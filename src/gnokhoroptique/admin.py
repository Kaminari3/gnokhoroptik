from django.contrib import admin

# Register your models here.
from .models import Client,Lunettes,Commande

admin.site.register(Client)
admin.site.register(Lunettes)
admin.site.register(Commande)