from django.urls import path
from django.conf.urls import url, include
from . import views
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from rest_framework import routers

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^clientadd/$', views.ClientListAddView.as_view()),
    url(r'^clientupdate/(?P<id>\d)/$', views.ClientUpdateView.as_view()),
    url(r'^clientdelete/(?P<pk>[0-9]+)/$', views.ClientDeleteView.as_view()),
    url(r'^lunetteadd/$', views.LunetteListAddView.as_view()),
    url(r'^lunetteupdate/(?P<id>\d)/$', views.LunetteUpdateView.as_view()),
    url(r'^lunettedelete/(?P<pk>[0-9]+)/$', views.LunetteDeleteView.as_view()),
    url(r'^commandeadd/$', views.CommandeListAddView.as_view()),
    url(r'^commandeupdate/(?P<id>\d)/$', views.CommandeUpdateView.as_view()),
    url(r'^commandedelete/(?P<pk>[0-9]+)/$', views.CommandeDeleteView.as_view()),
    url(r'^login/$', obtain_jwt_token),
    url(r'^clients/$', views.ClientListView.as_view()),
    url(r'^lunettes/$', views.LunetteListView.as_view()),
    url(r'^commandes/$', views.CommandeListView.as_view()), 
]