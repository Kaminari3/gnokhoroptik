from django.db import models

# Create your models here.

class Category (models.Model):
    name = models.CharField(max_length=30, blank=False, null=False)
    description = models.TextField(max_length=1000, blank=False, null=False)
    logo = models.FileField(blank=True, null=True)

    def __str__(self):
    	return str(self.name) + ' '


class Article(models.Model):
    category= models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=30, blank=False, null=False)
    description = models.TextField(max_length=1000, blank=False, null=False)
    logo = models.FileField(blank=True, null=True)

    def __str__(self):
    	return str(self.name) + ' '