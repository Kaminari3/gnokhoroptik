from django.db import models

# Create your models here.
class Product (models.Model):
    prod_name = models.CharField(max_length=30, blank=False, null=False)
    prod_desc = models.CharField(max_length=30, blank=False, null=False)
    prod_price = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
    	return str(self.prod_name) + ' '+str(self.prod_desc)+ ' '+str(self.prod_price)