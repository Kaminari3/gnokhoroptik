from rest_framework import serializers
from blog.models import Category, Article


class CategorySerializer(serializers.ModelSerializer):
	class Meta:
		model = Category
		fields = ('id','name','description','logo',)




class ArticleSerializer(serializers.ModelSerializer):
	category = CategorySerializer()

	class Meta:
		model = Article
		fields = ('id','name','description','logo','category',)


	def create(self, validated_data):
		category_data = validated_data.pop('category')
		#pop() is an inbuilt function in Python that removes 
		#and returns last value from the list or the given index value
		#for category_data in Category:
		newArticle = Article.objects.create(**validated_data)
		category= Category.objects.create(newArticle=newArticle,**category_data)
		return newArticle





			