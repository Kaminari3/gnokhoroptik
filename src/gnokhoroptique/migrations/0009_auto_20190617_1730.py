# Generated by Django 2.2 on 2019-06-17 17:30

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('gnokhoroptique', '0008_auto_20190614_2204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commande',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 17, 17, 30, 22, 808945, tzinfo=utc)),
        ),
    ]
