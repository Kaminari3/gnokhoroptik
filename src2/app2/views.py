from django.shortcuts import render
from rest_framework import generics, permissions, status,viewsets
from app2.models import *
from app2.serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse
# Create your views here.


def index(request):
    return HttpResponse("Hello, world. Bienvenue dans mon blog!!.")

class ProductView(generics.CreateAPIView):
    """
    Provides basic CRUD functions for the User model
    """
    serializer_class= ProductSerializer
    def get (self,request,*args,**kwargs):
        product=Product.objects.all()
        
        if not product:
            return Response({
                "status": "failure",
                "message": "no such item",
            }, status=status.HTTP_404_NOT_FOUND)
            
        serializer = ProductSerializer(product,many=True)
        
        return Response({
            "status": "success",
            "message": "items successfully retrieved",
            "count": product.count(),
            "data": serializer.data
        }, status=status.HTTP_200_OK)
        
    def post (self,request,*args,**kwargs):
        
        serializer = ProductSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
				"status":"failure",
				"message": serializer.errors,
				"errors": "erreur",
                "data":serializer.data
				}, status= status.HTTP_400_BAD_REQUEST)
   
        serializer.save()
        return Response({
            "data":serializer.data,        
            "status":"success",
			"message": "items successfully added",			
        }, status=status.HTTP_201_CREATED)
        
        

class ProductDetailByIdView(generics.CreateAPIView):
    
    serializer_class= ProductSerializer
    def get (self,request,*args,**kwargs):
        product=Product.objects.get(id=kwargs['id'])                    
        serializer = ProductSerializer(product)
        
        return Response( {"data":serializer.data}, status=status.HTTP_200_OK)
    
    
    def put (self, request, *args, **kwargs):
        try:
            product = Product.objects.get(id=kwargs['id'])
        except product.DoesNotExist:
            return Response({
				"status":"failure",
				"message": "no such item",
			}, status=status.HTTP_400_BAD_REQUEST)

        product_data= {
			'prod_name': request.data['prod_name'],
			'prod_desc': request.data['prod_desc'],
            'prod_price': request.data['prod_price'],
		}

        serializer = ProductSerializer(product, data=product_data, partial=True)

        if not serializer.is_valid():
            return Response({
				"status": "failure",
				"message": "invalid data",
				"errors": serializer.errors
				}, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
			"status":"success",
			"message": "item successfully update",
			"data": serializer.data
			}, status=status.HTTP_200_OK)
        
    def delete(self, request, *args, **kwargs):
        try:
            product = Product.objects.get(id=kwargs['id'])
        except product.DoesNotExist:
            return Response({
				"status": "failure",
				"message": "no such item"
				}, status=status.HTTP_400_BAD_REQUEST)

        product.delete()
        return Response({
			"status": "success",
			"message": "product successfully deleted"
			}, status=status.HTTP_200_OK)
    
    




