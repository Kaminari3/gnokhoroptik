from django.shortcuts import render

from django.http import HttpResponse
from rest_framework import generics, permissions, status
from blog.models import *
from blog.serializers import *
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.views import APIView


def index(request):
    return HttpResponse("Hello, world. Bienvenue dans mon blog!!.")

class ArticleListCreateView(generics.CreateAPIView):

	permissions_classes = (permissions.IsAuthenticated,)
	serializer_class = ArticleSerializer

	def get (self, request, *args, **kwargs):
		article = Article.objects.all()

		if not article:
			return Response({
				"status":"failure",
				"message": "no such item",
				}, status= status.HTTP_404_NOT_FOUND)

		serializer = ArticleSerializer(article, many=True)


		return Response({
			"status":"success",
			"message": "items successfully retrieved",
			"count": article.count(),
			"data": serializer.data
			}, status=status.HTTP_200_OK)


	def post (self, request, *args, **kwargs):
		
		serializer = ArticleSerializer(data=request.data)
		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": serializer.errors,
				"errors": "erreur"
				}, status= status.HTTP_400_BAD_REQUEST)

		
		
		serializer.create()


		return Response({
			"status":"success",
			"message": "items successfully created",
			
		}, status=status.HTTP_201_CREATED)


class ArticleUpdateView(generics.CreateAPIView):

	serializer_class = ArticleSerializer

	def put (self, request, *args, **kwargs):
		try:
			article = Article.objects.get(id=kwargs['id'])
		except Article.DoesNotExist:
			return Response({
				"status":"failure",
				"message": "no such item",
			}, status=status.HTTP_400_BAD_REQUEST)

		article_data= {
			'name': request.data['name'],
			'description': request.data['description'],
		}

		serializer = ArticleSerializer(article, data=article_data, partial=True)

		if not serializer.is_valid():
			return Response({
				"status": "failure",
				"message": "invalid data",
				"errors": serializer.errors
				}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status":"success",
			"message": "item successfully update",
			"data": serializer.data
			}, status=status.HTTP_200_OK)


class ArticleDeleteView (generics.CreateAPIView):
	
	serializer_class = ArticleSerializer

	def post(self, request, *args, **kwargs):
		try:
			article = Article.objects.get(id=kwargs['id'])
		except Article.DoesNotExist:
			return Response({
				"status": "failure",
				"message": "no such item"
				}, status=status.HTTP_400_BAD_REQUEST)

		article.delete()
		return Response({
			"status": "success",
			"message": "article successfully deleted"
			}, status=status.HTTP_200_OK)

		

class CategoryListCreateView(generics.CreateAPIView):

	#permissions_classes = (permissions.IsAuthenticated,)
	serializer_class = CategorySerializer

	def get (self, request, *args, **kwargs):
		category = Category.objects.all()

		if not category:
			return Response({
				"status":"failure",
				"message": "no such item",
				}, status= status.HTTP_404_NOT_FOUND)

		serializer = CategorySerializer(category, many=True)


		return Response({
			"status":"success",
			"message": "items successfully retrieved",
			"count": category.count(),
			"data": serializer.data
			}, status=status.HTTP_200_OK)


	def post (self, request, *args, **kwargs):
		
		serializer = CategorySerializer(data=request.data)
		if not serializer.is_valid():
			return Response({
				"status":"failure",
				"message": serializer.errors,
				"errors": "erreur"
				}, status= status.HTTP_400_BAD_REQUEST)

		serializer.save()


		return Response({
			"status":"success",
			"message": "items successfully created",
			
		}, status=status.HTTP_201_CREATED)


class CategoryUpdateView(generics.CreateAPIView):

	serializer_class = CategorySerializer

	def put (self, request, *args, **kwargs):
		try:
			category = Category.objects.get(id=kwargs['id'])
		except Category.DoesNotExist:
			return Response({
				"status":"failure",
				"message": "no such item",
			}, status=status.HTTP_400_BAD_REQUEST)

		category_data= {
			'name': request.data['name'],
			'description': request.data['description'],
		}

		serializer = CategorySerializer(category, data=category_data, partial=True)

		if not serializer.is_valid():
			return Response({
				"status": "failure",
				"message": "invalid data",
				"errors": serializer.errors
				}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status":"success",
			"message": "item successfully update",
			"data": serializer.data
			}, status=status.HTTP_200_OK)


class CategoryDeleteView (generics.CreateAPIView):
	
	serializer_class = CategorySerializer

	def post(self, request, *args, **kwargs):
		try:
			category = Category.objects.get(id=kwargs['id'])
		except Category.DoesNotExist:
			return Response({
				"status": "failure",
				"message": "no such item"
				}, status=status.HTTP_400_BAD_REQUEST)

		category.delete()
		return Response({
			"status": "success",
			"message": "article successfully deleted"
			}, status=status.HTTP_200_OK)

		

class CategoryById(generics.ListAPIView):
	
	serializer_class = CategorySerializer
	queryset = Category.objects.all()
	filter_backends = (DjangoFilterBackend,)
	filter_fields =('id',)

class ArticleById(generics.ListAPIView):
	serializer_class = ArticleSerializer
	queryset = Article.objects.all()
	filter_backends = (DjangoFilterBackend,)
	filter_fields =('id',)

class ArticleByCategory(generics.ListAPIView):
	serializer_class =ArticleSerializer
	queryset = Article.objects.all()
	filter_backends = (DjangoFilterBackend,)
	filter_fields = ('category__name',) #le nom de la categorie est
	#un sous ensemble de categorie et n'est pas present
	#dans article donc on y fait appel de cette façon
		

	
						

	
						
