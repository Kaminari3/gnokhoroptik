from rest_framework import serializers
from app2.models import Product

class ProductSerializer(serializers.ModelSerializer):
	class Meta:
		model = Product
		fields = ('id','prod_name','prod_desc','prod_price',)