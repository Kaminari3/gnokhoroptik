from django.shortcuts import render
from rest_framework import generics, permissions, status,viewsets
from app2.models import *
from app2.serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse
# Create your views here.


def index(request):
    return HttpResponse("Hello, world. Bienvenue dans mon blog!!.")

class ProductView(generics.CreateAPIView):
    """
    Provides basic CRUD functions for the User model
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

