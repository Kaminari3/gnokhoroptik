from django.db import models
from django.utils import timezone

# Create your models here.
class Client (models.Model):
    nom = models.CharField(max_length=30, blank=False, null=False)
    prenom = models.CharField(max_length=30, blank=False, null=False)
    adresse = models.TextField(max_length=1000, blank=False, null=False)
    telephone = models.CharField(max_length=20, blank=False, null=False)
    photo = models.FileField(blank=True, null=True)

    def __str__(self):
    	return str(self.nom) +' '+str(self.prenom)+' '+str(self.adresse)+' '+str(self.telephone)+' '+str(self.photo)


class Lunettes (models.Model):
    nom = models.CharField(max_length=30, blank=False, null=False)
    type = models.CharField(max_length=30, blank=False, null=False)
    prix = models.DecimalField(max_digits=8, decimal_places=2)
    photos = models.FileField(blank=True, null=True)

    def __str__(self):
    	return str(self.nom) +' '+str(self.type)+' '+str(self.prix)+' '+str(self.photos)

class Commande (models.Model):
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE, blank=True, null=True)
    lunette_id = models.ForeignKey(Lunettes, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField(default=timezone.now())
    nbre_lunettes = models.PositiveIntegerField(default=0)
    montant_total = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
    	return str(self.client_id) +' '+str(self.lunette_id)+' '+str(self.date)+' '+str(self.nbre_lunettes)+' '+str(self.montant_total)